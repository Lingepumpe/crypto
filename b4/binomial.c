#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// clang -Wall -O2 binomial.c -lgsl -lgslcblas -lm -o binomial
// ./binomial 1000
// GSL_RNG_TYPE="taus" GSL_RNG_SEED=123 ./binomial 1000

int main( int argc, char *argv[]) {
	if(argc != 2) {
		printf("Usage: %s numberOfSamples\n", argv[0]);
		exit(0);
	}
	
	// n = number of samples
	long long n = strtoll (argv[1],NULL,0);

	/* create a generator chosen by the
	   environment variable GSL_RNG_TYPE */
	gsl_rng_env_setup();
	const gsl_rng_type *T = gsl_rng_default;
	gsl_rng *r = gsl_rng_alloc (T);
	printf ("generator type: %s\n", gsl_rng_name (r));
	printf ("seed = %lu\n", gsl_rng_default_seed);

	/* generate n random variates chosen from
	   the binomial distribution and compute
	   mean and standard deviation */

	int numberTrials = 20;
	double p = 1/5.;
	
	double sumxi = 0.0;
	double sumxi2 = 0.0;
	for (int i = 0; i < n; i++) {
		double xi = gsl_ran_binomial(r, p, numberTrials);
		sumxi += xi;
		sumxi2 += xi*xi;
	}
	double xbar = sumxi/n;
	double sample_var  = (sumxi2-n*xbar*xbar)/(n-1);

	printf("sample mean       = %5.2f\n", xbar);
	printf("mean              = %5.2f\n", numberTrials*p);
	printf("sample variance   = %5.2f\n", sample_var);
	printf("variance          = %5.2f\n",
           numberTrials*p*(1-p));
		
	gsl_rng_free (r);
	return 0;
}
