%----------------------------------------------------------------------------------------
% Blatt 5
% UIBK
% 'Blatt 5', Informationstheorie und Kryptologie
% Peter Kirk
%----------------------------------------------------------------------------------------

\documentclass[11pt,a4paper]{article}

%----------------------------------------------------------------------------------------
% Include required packages
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{url}
\usepackage{xcolor}
\usepackage{wasysym}
\usepackage{ifxetex}
\usepackage[left=2.7cm, right=2.7cm, top=3cm]{geometry}

\usepackage{tikz}
\usepackage{listings}


\ifxetex
  \usepackage[log-declarations=false]{xparse}
  \usepackage[quiet]{fontspec}
  \usepackage{polyglossia}
  %% \setmainlanguage{english}
  \setmainlanguage{german}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  %% \usepackage[english]{babel}
  \usepackage[ngerman]{babel}
  \usepackage{lmodern}
\fi


%% make sure tables are not cramped vertically
\renewcommand{\arraystretch}{1.2}



%----------------------------------------------------------------------------------------
% Start document

\begin{document}


%----------------------------------------------------------------------------------------
% Title page
%----------------------------------------------------------------------------------------

\begin{titlepage} % User-defined title page

\begin{center}
\includegraphics[width=0.12\textwidth]{images/uibk}

\begin{large}
Leopold-Franzens-Universität Innsbruck\\[5mm]
Institut für Mathematik\\[25mm]
\end{large}

{\LARGE \bf Blatt 5\\[2mm]}

Informationstheorie und Kryptologie\\ 
Proseminar\\[15mm]

Peter Kirk\\[35mm]

Betreuer\\
Arne Dür\\[10mm]

\vfill

Innsbruck, \today
\end{center}

\end{titlepage}


\section{Aufgabe 12}
Wie man in Abbildung \ref{fig:gtrends} sieht, ist Octave/Matlab nicht so
Hip wie R, also erledigen wir die Aufgabe doch lieber in R.

\begin{figure}[ht]
  \centering
  \includegraphics[width=10cm]{images/gtrends}
  \caption{Google Trends zu R, Matlab und Octave}
  \label{fig:gtrends}
\end{figure}

Die Datei entropy.R erfüllt die Anforderungen:

\lstinputlisting[language=R]{entropy.R}

Der Output ist:

\begin{verbatim}
> H(4/8, 0, 4/8)
[1] 1
> H(2/8, 4/8, 2/8)
[1] 1.5
> H(3/8, 2/8, 3/8)
[1] 1.561278
\end{verbatim}

Die größte der drei Entropien ist offensichtlich die dritte. Der maximale Wert für H mit drei Parametern ist, laut Satz 2.2, $H(1/3, 1/3, 1/3) = ld(3)$. Dieses Maximum ist eindeutig, denn laut Satz 2.2 ist das Maximum ``genau dann'' erreicht. 


\section{Aufgabe 13}

Sei der Ergebnissraum $\Omega = \{1+, 1-, 2+, 2-, \ldots, 12+, 12-\}$. Das Ergebniss 4+ bedeutet hier etwa dass die Münze 4 die gesuchte Münze ist,
und dass sie schwerer ist als die anderen.

Es gibt $n = 12$ Münzen. Wir wählen zwei Mal m Münzen aus, und legen diese jeweils links bzw. rechts auf die Wage. Es gibt folgende Möglichkeiten:

\begin{enumerate}
\item $m = 1$: Es werden 1+1 Münzen gewogen, und 10 bleiben liegen.
\item $m = 2$: Es werden 2+2 Münzen gewogen, und 8 bleiben liegen.
\item $m = 3$: Es werden 3+3 Münzen gewogen, und 6 bleiben liegen.
\item $m = 4$: Es werden 4+4 Münzen gewogen, und 4 bleiben liegen.
\item $m = 5$: Es werden 5+5 Münzen gewogen, und 2 bleiben liegen.
\item $m = 6$: Es werden 6+6 Münzen gewogen, keine bleibt liegen.
\end{enumerate}

Wir nehmen eine Gleichverteilung in $\Omega$ an, es gibt also 24 mögliche
Ergebnisse unseres Zufallsexperimentes die alle Warscheinlichkeit $\frac{1}{24}$ haben: Jede der 12 Münzen kann die
gesuchte sein, und kann entweder leichter oder schwerer als die anderen sein.

Für die erste Wiegung stehen uns die oben angegeben 6 möglichen Tests zur Verfügung. Wir wählen nach Shannon-Fano
denjenigen mit höchster Entropie. Jeder der Tests hat drei mögliche Ausgänge (die Ergebnisse der Wage): Gleichheit,
linke Seite schwerer und rechte Seite schwerer, im Folgenden als ``='', ``l'' und ``r'' bezeichnet.

\begin{enumerate}
\item ``='' ist das Warscheinlichste, mit $P(X_1 = \text{``=''}) = \frac{20}{24}$; ``l'' und ``r'' haben jeweils $\frac{2}{24}$. Es ergibt sich
  also $H(X_1) = -\frac{20}{24}*ld(\frac{20}{24}) - 2*\frac{2}{24}*ld(\frac{2}{24}) \approx 0.82 bit$
\item $H(X_1) = H(\frac{16}{24}, \frac{4}{24}, \frac{4}{24}) \approx 1.25 bit$
\item $H(X_1) = H(\frac{12}{24}, \frac{6}{24}, \frac{6}{24}) \approx 1.5 bit$
\item $H(X_1) = H(\frac{8}{24}, \frac{8}{24}, \frac{8}{24}) \approx 1.58 bit$
\item $H(X_1) = H(\frac{4}{24}, \frac{10}{24}, \frac{10}{24}) \approx 1.48 bit$
\item $H(X_1) = H(\frac{0}{24}, \frac{12}{24}, \frac{12}{24}) \approx 1 bit$
\end{enumerate}

Nach dem Prinzip der Entropiemaximierung wählen wir also für $X_1$ Möglichkeit 4, legen also Münzen 1-4 auf die linke Seite der Wage, Münzen 5-8 auf die rechte Seite und
lassen Münzen 9-12 an der Seite liegen. Es gibt drei mögliche Ausgänge des Experimentes, die wir getrennt betrachten.

\subsection{$X_1 = \text{``=''}$}
Bei Gleichheit wissen wir, dass die gesuchte Münze in den nicht gewogenen Münzen 9-12 sein muss, also bleibt $B = \{9-, 9+, 10-, 10+, 11-, 11+, 12-, 12+\}$. Wir
wählen jetzt unseren zweiten Test $X_2$, so dass $H(X_2 | B)$ maximal wird.

Die Entropiestärkste Verteilung der 8 Ergebnisse auf die drei Testergebnisse ist, wie in Aufgabe 12 bestimmt, $H(\frac{3}{8}, \frac{3}{8}, \frac{2}{8})$, wir müssen
nur noch herausfinden wie die Wage beladen werden muss um diese Warscheinlichkeiten für die Ausgänge zu erzeugen.

Legen wir Münze 9 zur Seite, und Münzen 10, 11 und 12 auf die linke Seite der Waage und Münzen 1, 2 und 3 auf die rechte Seite haben wir die gewünschten Warscheinlichkeiten.

\subsubsection{$X_2 = \text{``=''}$}
Bei Gleichheit ist die gesuchte Münze die Münze 9, eine weitere Wiegung wird herausbringen ob das Ergebniss 9+ oder 9- vorliegt.

\subsubsection{$X_2 = \text{``l''}$}
Ist die linke Seite schwerer, so haben wir $B = \{10+, 11+, 12+\}$. Für $X_3$ wiegen wir Münze 10 links gegen Münze 11 rechts und kommen so
auf das Ergebniss (ist links schwerer war es 10+, ist rechts schwerer 11+, bei Gleichheit 12+)

\subsubsection{$X_2 = \text{``r''}$}
Analog zu $X_2 = \text{``l''}$.

\subsection{$X_1 = \text{``l''}$}
Bei Ungleichheit wissen wir, dass die gesuchte Münze in den ersten 1-8 Münzen sein muss. Weiterhin wissen wir, dass die gesuchte Münze entweder
schwerer und auf der abgesenkten Seite ist, oder leichter und auf der erhöhten Seite ist. Also bleibt $B = \{1+, 2+, 3+, 4+, 5-, 6-, 7-, 8-\}$.

Wir suchen wieder ein Test $X_2$ der uns $H(X_2 | B) = H(\frac{3}{8}, \frac{3}{8}, \frac{2}{8})$ gibt. Dazu lassen wir 1,2 und 3 an der Seite liegen,
legen 5, 9, 10 und 11 links, und 4, 6, 7 und 8 rechts. Gleichheit tritt in 3 von 8 Fällen auf (wenn es Münze 1, 2 oder 3 war), die linke Seite ist
schwerer in 3 von 8 Fällen (wenn es Münze 6, 7 oder 8 war). Und schliesslich die rechte Seite ist schwerer in 2 aus 8 Fällen (wenn es Münze 4 oder
5 war).

\subsubsection{$X_2 = \text{``=''}$}
Bei Gleichheit bleibt also $B = \{1+, 2+, 3+\}$. Wiegen wir nun 1 links und 2 rechts, so ist das Ergebniss eindeutig bestimmt: Bei ``='' war es
3+, bei ``l'' war es 1+ und bei ``r'' war es 2+.

\subsubsection{$X_2 = \text{``l''}$}
Ist die linke Seite schwerer so bleibt $B = \{6-, 7-, 8-\}$. Dieser Fall ist Symetrisch zu dem bei $X_2 = ``=''$.

\subsubsection{$X_2 = \text{``r''}$}
Ist die rechte Seite schwerer so bleibt $B = \{4+, 5-\}$, ein wiegen von 1 gegen 4 entscheidet das Ergebniss eindeutig.

\subsection{$X_1 = \text{``r''}$}
Wird analog zu $X_1 = \text{``l''}$ behandelt.

\subsection{3 Wägungen für 13 Münzen?}
Im ersten Test werden wir analog wieder Münzen 1-4 links und Münzen 5-8 rechts auflegen - es bleiben hier natürlich
dann 5 liegen, diese Konfiguration ist nach der Entropiemaximierung die günstigste. Die Fälle $X_1 = \text{``l''}$
bzw. $X_1 = \text{``r''}$ lösen sich wie oben auf, bei Gleichheit haben wir aber $B = \{9-, 9+, \ldots, 13-, 13+\}$
übrig. Das sind 10 mögliche Ergebnisse die wir unterscheiden sollen, aber es bleiben nurnoch zwei Wiegungen, und diese
zwei Wiegungen haben nur $3^2 = 9$ mögliche Ausgänge - offensichtlich können sie also nicht ausreichen um die 10 Ergebnisse
sicher zu trennen. 


\end{document}
