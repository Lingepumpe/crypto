def decodeNumber(e):
    #returns a list of the decoded numbers, or "Error"
    #when the encoding was not valid
    result = []
    while(len(e) > 0):
        n = 3 #the first binary number is padded to 3 byte length
        niceString = ""
        while 1:
            try:
                #as the encoded string has a format similar to:
                #f(3)f(4)f(2015), we cut off the first f
                nextN = int(e[:n], 2)
                niceString += e[:n] + " "
                e = e[n:]
                n = nextN
                if e[0] == '0':
                    print "Parsed a number: " + str(n)
                    e = e[1:]
                    result.append(n)
                    break
            except:
                    print "Parse path: " + niceString + "; "
                    return "Error"
    return result

def main():
    testNumbers = ['100101111111011111010', \
                   '100101111111011111010101110',\
                   '1001011110000110000', '1010', '11100110' ]
    for t in testNumbers:
        print "Decoding %30s" % t
        print "Result: %s\n" % decodeNumber(t)

main()
