#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// gcc -O normal.c -lgsl -lgslcblas -lm -o normal
// ./normal 1000
// GSL_RNG_TYPE="taus" GSL_RNG_SEED=123 ./normal 1000

int
main( int argc, char *argv[])
{
  const gsl_rng_type *T;
  gsl_rng *r;
  long long i, n;
  double xi,sumxi,sumxi2,xbar,s,t;

  // n = number of samples
  n = strtoll (argv[1],NULL,0);

  /* create a generator chosen by the
     environment variable GSL_RNG_TYPE */
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  printf ("generator type: %s\n", gsl_rng_name (r));
  printf ("seed = %lu\n", gsl_rng_default_seed);
  printf ("first value = %lu\n", gsl_rng_get (r));

  /* generate n random variates chosen from
     the normal distribution and compute
     mean and standard deviation */
  sumxi = 0.0;
  sumxi2 = 0.0;
  for (i = 0; i < n; i++) {
    xi = gsl_ran_gaussian (r,1.0);
    sumxi += xi;
    sumxi2 += xi*xi;
  }
  xbar = sumxi/n;
  s = sqrt((sumxi2-n*xbar*xbar)/(n-1));
  t = sqrt((sumxi2-n*xbar*xbar)/n);
  printf ("xbar= %g\n",xbar);
  printf ("s= %g\n",s);
  printf ("t= %g\n",t);

  gsl_rng_free (r);
  return 0;
}
