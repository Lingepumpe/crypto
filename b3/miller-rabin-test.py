def isProbablyPrimeMillerRabin(n, a):
    assert(a >= 1 and a < n)
    u = n - 1
    e = 0
    while u % 2 == 0:
        u /= 2
        e += 1

    for i in range(e - 1, -1, -1):
        if pow(a, pow(2, i)*u) % n == n-1:
            return "prime"
    if pow(a, u) % n == 1:
        return "prime"
    return "non-prime"

def testAs(n):
    prime_result_as = []
    nonprime_result_as = []
    print "Testing " + str(n)
    for a in xrange(1, n):
        if isProbablyPrimeMillerRabin(n, a) == "prime":
            prime_result_as.append(a)
        else:
            nonprime_result_as.append(a)
    print ("Prime detected with these a's: " + str(prime_result_as) +
          "; " + str(len(prime_result_as)) + "/" + str(n-1) + " a's")
    print ("Non-Prime detected with these a's: " + str(nonprime_result_as) +
          "; " + str(len(nonprime_result_as)) + "/" + str(n-1) + " a's")
    print


testAs(89)
testAs(87)
