%----------------------------------------------------------------------------------------
% Blatt 8
% UIBK
% 'Blatt 8', Informationstheorie und Kryptologie
% Peter Kirk
%----------------------------------------------------------------------------------------

\documentclass[11pt,a4paper]{article}

%----------------------------------------------------------------------------------------
% Include required packages
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{url}
\usepackage{xcolor}
\usepackage{wasysym}
\usepackage{ifxetex}
\usepackage[left=2.7cm, right=2.7cm, top=3cm]{geometry}

\usepackage{tikz}
\usepackage{listings}


\ifxetex
  \usepackage[log-declarations=false]{xparse}
  \usepackage[quiet]{fontspec}
  \usepackage{polyglossia}
  %% \setmainlanguage{english}
  \setmainlanguage{german}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  %% \usepackage[english]{babel}
  \usepackage[ngerman]{babel}
  \usepackage{lmodern}
\fi


%% make sure tables are not cramped vertically
\renewcommand{\arraystretch}{1.2}



%----------------------------------------------------------------------------------------
% Start document

\begin{document}


%----------------------------------------------------------------------------------------
% Title page
%----------------------------------------------------------------------------------------

\begin{titlepage} % User-defined title page

\begin{center}
\includegraphics[width=0.12\textwidth]{images/uibk}

\begin{large}
Leopold-Franzens-Universität Innsbruck\\[5mm]
Institut für Mathematik\\[25mm]
\end{large}

{\LARGE \bf Blatt 8\\[2mm]}

Informationstheorie und Kryptologie\\ 
Proseminar\\[15mm]

Peter Kirk\\[35mm]

Betreuer\\
Arne Dür\\[10mm]

\vfill

Innsbruck, \today
\end{center}

\end{titlepage}


\section{Aufgabe 18}
Zuerst bestimmen wir die Huffmann-Codierung für die gegebene Informationsquelle,
wir folgen dabei dem Pseudocode in Definition 2.11. $U = \{c, a, b\}$ mit
elementaren Warscheinlichkeiten $\frac{3}{5} \geq \frac{1}{5} \geq \frac{1}{5}$.


\subsection{Einzelcodierung nach Huffmann}
\begin{itemize}
\item Wir kombinieren a und b, es ergibt sich: $U = \{c, ab\}$ mit
elementaren Warscheinlichkeiten $\frac{3}{5} \geq \frac{2}{5}$.
\item Wir kombinieren c und ab, es ergibt sich: $U = \{abc\}$ mit
  elementaren Warscheinlichkeit $1$.
\end{itemize}
Nun betrachten wir den erzeugten Code:
\begin{itemize}
\item $h(c) = h'(c) = h''(abc)0 = \epsilon0 = 0$
\item $h(a) = h'(ab)0 = h''(abc)10 = \epsilon10 = 10$
\item $h(b) = h'(ab)1 = h''(abc)11 = \epsilon11 = 11$
\end{itemize}

Die Kompressionsrate entspricht der erwarteten Codewortlänge pro Quellsymbol.
Diese ist $0.6*1 + 2*0.2*2 = 1.4$
  
\subsection{Paarcodierung nach Huffmann}
Hier ist $U = \{cc, ca, ac, cb, bc, aa, ab, ba, bb\}$ mit elementaren Warscheinlichkeiten
$\frac{9}{25} \geq \frac{3}{25} \geq \frac{3}{25} \geq \frac{3}{25} \geq \frac{3}{25} \geq \frac{1}{25}
\geq \frac{1}{25} \geq \frac{1}{25} \geq \frac{1}{25}$.

Es ergibt sich:
\begin{itemize}
\item $h(bb) = h'(babb)1$
\item $h(ba) = h'(babb)0$
\item $h(ab) = h^{(2)}(aaab)1$
\item $h(aa) = h^{(2)}(aaab)0$
\item $h^{(1)}(babb) = h^{(3)}(bcbabb)1$
\item $h(bc) = h^{(3)}(bcbabb)0$
\item $h^{(2)}(aaab) = h^{(4)}(cbaaab)1$
\item $h(cb) = h^{(4)}(cbaaab)0$
\item $h(ac) = h^{(5)}(caac)1$
\item $h(ca) = h^{(5)}(caac)0$
\item $h^{(3)}(bcbabb) = h^{(6)}(cbaaabbcbabb)1$
\item $h^{(4)}(cbaaab) = h^{(6)}(cbaaabbcbabb)0$
\item $h^{(5)}(caac) = h^{(7)}(cccaac)1$
\item $h(cc) = h^{(7)}(cccaac)0$
\item $h^{(6)}(cbaaabbcbabb) = h^{(8)}(cccaaccbaaabbcbabb)1$
\item $h^{(6)}(cccaac) = h^{(8)}(cccaaccbaaabbcbabb)0$
\item $h^{(8)}(cccaaccbaaabbcbabb) = \epsilon$
\end{itemize}

Abschliessend ergibt sich also bei der Paarcodierung:

\begin{center}
  \begin{tabular}{ c | c | c | c | c | c | c | c | c }
    cc & ca & ac & cb & bc & aa & ab & ba & bb\\ \hline
    00 & 010 & 011 & 100 & 110 & 1010 & 1011 & 1110 & 1111 \\
  \end{tabular}
\end{center}

Die Kompressionsrate entspricht der erwarteten Codewortlänge pro Quellsymbol.
Diese ist $(\frac{9}{25}*2 + 4*\frac{3}{25}*3 + 4*\frac{1}{25}*4)/2 = 1.4$

\subsection{Theoretische Schranken}
Nach Satz 2.18 gibt es eine untere und eine obere Schranke für die Kompressionsrate
nach Huffmann. Sie ist nach unten durch H und nach oben durch $H + 1$ bei Einzelcodierung bzw. $H + \frac{1}{2}$ bei Paarcodierung gegeben.

Für unsere Quelle gilt $H = H(3/5, 1/5, 1/5) \approx 1.370951$. Also gilt für die Einzelcodierung $1.370951 \leq \text{Kompressionsrate} \leq 2.370951$, für die
Paarcodierung $1.370951 \leq \text{Kompressionsrate} \leq 1.870951$. Wir sind also mit $1.4$ in beiden Fällen schon ziemlich gut dran, erst die Codierung von
35-Tupeln gibt uns nach der Theorie eine garantiert bessere Encodierung.


\section{Aufgabe 19}
Die Ausgabe der LZ Codierung sind Trippel (Abstand, Länge, Symbol), wobei die ersten zwei Zahlen jeweils mit der
cf Codierung codiert werden, das Symbol wird als Blockcodierung codiert, im Binären Fall also direkt übernommen.

Zur Länge von mit der cf Codierung codierten Zahlen bemerken wir: cf(x) für $0 \leq x \leq 7$ ist 4 Bit
lang. Für cf(8) ist die Länge 8 Bit. Asymptotisch ist die Länge der Standardcodierung und der kommafreien Codierung
gleich, d.h. die Länge der cf Codierung ist asymptotisch logarithmisch.

\subsection{$101001101110000$}
Für die Tripel ergibt sich: $(0,0,1)$, $(0,0,0)$, $(2,2,0)$, $(3,1,1)$, $(3,3,1)$, $(8,2,0)$ und $(0,0,0)$.
Also ist die LZ Codierung:
\[
cf(0)cf(0)1cf(0)cf(0)0cf(2)cf(2)0cf(3)cf(1)1cf(3)cf(3)1cf(8)cf(2)0cf(0)cf(0)0
\]

Wir sparen uns hier die kommafreie Codierung explizit aufzuschreiben, sie wurde bereits in Blatt 7
behandelt.

Zur Decodierung ergeben die ersten zwei Trippel $10$, das dritte Trippel hängt $100$ an, das vierte Tripel
hängt $11$ an, das fünfte Tripel hängt $0111$ an, das sechste Tripel hängt $000$ an, und schliesslich das letzte
Tripel $0$. Die Decodierung resultiert also in $101001101110000$, unserem Startwort.

Die LZ Codierung ist $6*(4+4+1) + (8+4+1) = 67$ Bit lang. Das Eingabewort ist jedoch nur 15 Bit lang, so ist
also die Kompressionsrate etwa $0.2238$, d.h. die Codierung ist deutlich länger als das Original.

\subsection{$1^n$}
Für $n > 2$ sind die Tripel: $(0,0,1)$ und $(1,n-2,1)$. Die LZ Codierung ist also
\[
cf(0)cf(0)1cf(1)cf(n-2)1
\]

In der Decodierung ergibt das erste Tripel $1$, das zweite Trippel hängt $1^{n-1}$ hinzu. Es ergibt sich also wieder
unser Startwort, $1^{n}$.

Die LZ Codierung ist $(4+4+1) + (4+l(cf(n-2))+1) = 14+l(cf(n-2))$ Bits lang. Asymptotisch ist also die Länge dieser
LZ Codierung $14+log(n-2)$. Die Länge der Eingabe ist n, also geht die Kompressionsrate in diesem Fall gegen $\infty$.


\subsection{$101^n$}
Für $n \geq 2$ sind die Tripel: $(0,0,1)$, $(0,0,0)$, $(2,1,1)$ und $(3,3n-5,1)$. In LZ Codierung sind die Tripel
dann:
\[
cf(0)cf(0)1cf(0)cf(0)0cf(2)cf(1)1cf(3)cf(3n-5)1
\]

Die Decodierung erzeugt in den ersten zwei Tripeln $10$, Tripel drei hängt $11$ an, Tripel vier hängt $01(101)^{n-2}$ hinzu. Insgesamt
haben wir also $(101)^n$, wie gewünscht.

Die LZ Codierung hat die Länge $3*(4+4+1) + (4+l(cf(3n-5))+1) = 32 + l(cf(3n-5))$ Bit. Die Länge der Eingabe ist hier $3*n$, so geht
also asymptotisch die Kompressionsrate wieder gegen $\infty$, da die Länge der Codierung logarithmisch in n wächst, während die Länge der Eingabe
linear in n wächst.
\end{document}
