from itertools import tee, islice
import re
import collections
import string
import math
import operator

def readChunk(fd):
    while True:
        data = fd.read(16384)
        if not data:
            break
        yield data

def ngrams(lst, n):
    tlst = lst
    while True:
        a, b = tee(tlst)
        l = tuple(islice(a, n))
        if len(l) == n:
            yield l
            next(b)
            tlst = b
        else:
            break

def normalizingH(lst):
    result = 0
    nsum = float(sum(lst))
    for v in lst:
        if v != 0:
            p = v / nsum 
            result = result - (p*math.log(p, 2))
    return result

def main():
    f = open('book.txt')
    grams  = [1, 2, 3]
    gns     = [collections.Counter() for n in grams]
    for chunk in readChunk(f):
        chunk = chunk
        #stop double white spaces
        chunk = re.sub('\s+', ' ', chunk)
        #remove everything except a-z and space, converted to lower
        remove_chars = chunk.translate(None, string.letters + " ")
        chunk = chunk.translate(None, remove_chars).lower()
        
        letters = list(chunk)
        for i in xrange(0, len(grams)):
            gns[i].update(list(ngrams(letters, grams[i])))
    for i in xrange(0, len(grams)):
        sums = sum(gns[i].values())
        #print gns[i].most_common(10)
        mc = [a for (a, b) in gns[i].most_common(10)]
        mc = [reduce(operator.add, e) for e in mc]
        occ = [100.0*int(b)/sums for (a, b) in gns[i].most_common(10)]
        print "Most common " + str(grams[i]) + "-grams:",
        for (m, o) in zip(mc, occ):
            print " \'%s\' ~ %.2f%%," % (m, o),
        print ""
    for i in xrange(0, len(grams)):
        print "entropy " + str(grams[i]) + "-gram " + \
              str(normalizingH(gns[i].values()) / grams[i])



main()
